/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import Presentacion.Inicio;
  
/**
 *
 * @author Guerrero
 */
public class Datos {
    private String name;
    private String ae1;
    private String ae2;
    private String mon;
    
    public Datos(String name, String ae1, String ae2, String mon) {
        this.name = name;
        this.ae1 = ae1;
        this.ae2 = ae2;
        this.mon = mon;
    }
    
    public static ArrayList<Datos> listaApuestas = new ArrayList<>();
    public DefaultListModel AddModel = new DefaultListModel(); 
    public Datos() {
        try {
            File archivo = new File("ListaApuestas.txt");
            archivo.createNewFile();
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, "Error al crear el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        
    }

    public void crear(){
    try {
            File archivo = new File("ListaApuestas.txt");
            archivo.createNewFile();
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, "Error al crear el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
}
    
    public void mLis(){
       
        BufferedReader br = null;
    try{
        br = new BufferedReader(new FileReader("ListaApuestas.txt"));
        String line;
        while ((line = br.readLine()) != null){
            AddModel.addElement(line);
        }
        
    }
    catch(Exception e){
        System.out.println(""+e);
    }
    finally{
        try{
            br.close();
        }
        catch(Exception e){
            System.out.println(""+e);
        }
    }
        
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAe1() {
        return ae1;
    }

    public void setAe1(String ae1) {
        this.ae1 = ae1;
    }

    public String getAe2() {
        return ae2;
    }

    public void setAe2(String ae2) {
        this.ae2 = ae2;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }
            
}
